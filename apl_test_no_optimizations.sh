#!/bin/bash
JAR_PATH=$(grep "gameoflife" ./target/* -ls)
block_sizes=(32 64 128 256 512 1024)
n_cells=(10000 100000 1000000 10000000 100000000)
devices=(0 1)

for device in ${devices[*]}
do
for cells in ${n_cells[*]}
do
for block in ${block_sizes[*]}
do
#printf "\nB=%d N=%d D=%d" $block $cells $device
java -jar $JAR_PATH -n $cells -d $device -r 6 -b $block -e 100 -m $cells --no-optimizations
done
done
done



