package gui;
/**
 * Created by Kamil on 12.05.2017.
 */

import cuda.BaseGpuSimulation;
import cuda.CudaDriver;
import cuda.GpuPointers;
import javafx.embed.swing.SwingNode;
import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;

import javax.media.opengl.*;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import java.awt.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static jcuda.driver.CUgraphicsMapResourceFlags.CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD;
import static jcuda.driver.JCudaDriver.*;

public class SimulationDisplay implements GLEventListener {
    private final GLJPanel panel;
    private CUgraphicsResource graphicsResource;
    private BaseGpuSimulation gameOfLifeSimulation;
    private int[] gl_pixelBufferObject;
    private int[] gl_texturePtr;
    private int meshHeight;
    private int meshWidth;
    private float zoom;
    private int displacementX;
    private int displacementY;

    public void drawSimulation(BaseGpuSimulation gameOfLifeSimulation, float actualZoom, int displacementX, int displacementY) {
        this.gameOfLifeSimulation = gameOfLifeSimulation;
        this.zoom = actualZoom;
        this.displacementX = displacementX;
        this.displacementY = displacementY;
        panel.display();
    }

    public SimulationDisplay(SwingNode swingNode, GLCapabilities capabilities) {
        panel = new GLJPanel(capabilities);
        panel.setVisible(true);
        panel.setMinimumSize(new Dimension(800, 800));

        panel.addGLEventListener(this);
        swingNode.setContent(panel);
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        try {
            GL2 gl = drawable.getGL().getGL2();
            gl.setSwapInterval(0);
            setupView(drawable);
            createOpenGLCudaContext();
            initTextureBufferObject(gl, drawable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createOpenGLCudaContext() {
        CUdevice dev = new CUdevice();
        cuDeviceGet(dev, 0);
        CUcontext glCtx = new CUcontext();
        cuGLCtxCreate(glCtx, 0, dev);
    }

    private void initTextureBufferObject(GL2 gl, GLAutoDrawable drawable) {
        cleanCudaAndGl(gl);
        meshWidth = drawable.getWidth();
        meshHeight = drawable.getHeight();
        gl_texturePtr = new int[1];
        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glGenTextures(1, IntBuffer.wrap(gl_texturePtr));
        gl.glBindTexture(GL.GL_TEXTURE_2D, gl_texturePtr[0]);
        byte[] textureBufferData = new byte[4 * meshHeight * meshWidth];

        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        ByteBuffer glTextureBufferData = ByteBuffer.wrap(textureBufferData);
        gl.glTexImage2D(GL.GL_TEXTURE_2D, 0, GL.GL_RGBA8, meshWidth, meshHeight, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, glTextureBufferData);

        gl_pixelBufferObject = new int[1];
        gl.glGenBuffers(1, IntBuffer.wrap(gl_pixelBufferObject));
        gl.glBindBuffer(GL2GL3.GL_PIXEL_UNPACK_BUFFER, gl_pixelBufferObject[0]);
        gl.glBufferData(GL2GL3.GL_PIXEL_UNPACK_BUFFER, 4 * meshWidth * meshHeight * Sizeof.BYTE,
                glTextureBufferData, GL2GL3.GL_STREAM_COPY);

        graphicsResource = new CUgraphicsResource();
        cuGraphicsGLRegisterBuffer(graphicsResource, gl_pixelBufferObject[0], CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD);
    }

    private void cleanCudaAndGl(GL2 gl) {
        if (gl_texturePtr != null) {
            gl.glDeleteTextures(1, IntBuffer.wrap(gl_texturePtr));
            gl_texturePtr = null;
        }
        if (gl_pixelBufferObject != null) {
            gl.glDeleteBuffers(1, IntBuffer.wrap(gl_pixelBufferObject));
            gl_pixelBufferObject = null;
        }

        if (graphicsResource != null) {
            cuGraphicsUnregisterResource(graphicsResource);
            graphicsResource = null;
        }
    }

    private void setupView(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glViewport(0, 0, drawable.getWidth(), drawable.getHeight());

        gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
        gl.glLoadIdentity();

        gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(0.0, drawable.getWidth(), drawable.getHeight(), 0.0, -1.0, 1.0);
    }

    @Override
    public synchronized void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        runCuda(gl);
        drawTexture(drawable, gl);
    }

    private void drawTexture(GLAutoDrawable drawable, GL2 gl) {
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glBindTexture(GL.GL_TEXTURE_2D, gl_texturePtr[0]);
        gl.glBindBuffer(GL2GL3.GL_PIXEL_UNPACK_BUFFER, gl_pixelBufferObject[0]);
        gl.glTexSubImage2D(GL.GL_TEXTURE_2D, 0, 0, 0, drawable.getWidth(), drawable.getHeight(),
                GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, 0);

        gl.glBegin(GL2.GL_QUADS);

        gl.glLoadIdentity();

        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(0.0f, 0.0f);

        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(drawable.getWidth(), 0.0f);

        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(drawable.getWidth(), drawable.getHeight());

        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(0.0f, drawable.getHeight());

        gl.glEnd();

        gl.glBindBuffer(GL2GL3.GL_PIXEL_UNPACK_BUFFER, 0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
    }

    private void runCuda(GL gl) {
        BaseGpuSimulation gameOfLifeSimulation = this.gameOfLifeSimulation;
        if (gameOfLifeSimulation == null) return;

        CUdeviceptr basePointer = mapGraphicsResourceToCuda();
        runDrawKernel(gameOfLifeSimulation, basePointer);
        unmapResource();
    }

    private void unmapResource() {
        cuGraphicsUnmapResources(1, new CUgraphicsResource[]{graphicsResource}, null);
    }

    private CUdeviceptr mapGraphicsResourceToCuda() {
        CUdeviceptr basePointer = new CUdeviceptr();
        cuGraphicsMapResources(1, new CUgraphicsResource[]{graphicsResource}, null);
        cuGraphicsResourceGetMappedPointer(basePointer, new long[1], graphicsResource);
        return basePointer;
    }

    private void runDrawKernel(BaseGpuSimulation simulation, CUdeviceptr basePointer) {
        Pointer paramsPointer = getDrawKernelParamsPointer(simulation.getSize(), simulation.getGpuPointers(), basePointer);
        int blockX = 8;
        int blockY = 8;
        int gridX = (int) Math.ceil((double) meshWidth / blockX);
        int gridY = (int) Math.ceil((double) meshHeight / blockY);

        CudaDriver.runKernel(simulation.getDrawKernel(), gridX, gridY, blockX, blockY, paramsPointer);
    }

    private Pointer getDrawKernelParamsPointer(int size, GpuPointers kernelBuffers, CUdeviceptr basePointer) {
        return Pointer.to(
                Pointer.to(basePointer),
                Pointer.to(new int[]{meshWidth}),
                Pointer.to(new int[]{meshHeight}),
                Pointer.to(new int[]{size}),
                Pointer.to(new float[]{zoom}),
                Pointer.to(new int[]{displacementX}),
                Pointer.to(new int[]{displacementY}),
                Pointer.to(kernelBuffers.getCellInputPointer())
        );
    }

    @Override
    public void reshape(final GLAutoDrawable drawable, int x, int y, int width,
                        int height) {
        if (width != meshWidth && height != meshHeight) {
            System.out.println("Reshape");
            GL2 gl = drawable.getGL().getGL2();
            setupView(drawable);
            initTextureBufferObject(gl, drawable);
        }
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        cleanCudaAndGl(drawable.getGL().getGL2());
    }
}