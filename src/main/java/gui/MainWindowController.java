package gui;

import cuda.BaseGpuSimulation;
import cuda.CudaDriver;
import cuda.GameOfLifeSimulation;
import cuda.OptimizedGameOfLifeSimulation;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Kamil on 13.05.2017.
 */
public class MainWindowController {
    @FXML
    Label cudaDeviceNameLabel;
    @FXML
    SwingNode swingNode;
    @FXML
    Slider worldSizeSlider;
    @FXML
    Button resetButton;
    @FXML
    Button nextEpochButton;
    @FXML
    Slider numOfThreadsSlider;
    @FXML
    Label epochLabel;
    @FXML
    Label numOfCellsLabel;
    @FXML
    Slider numOfEpochsSlider;
    @FXML
    GridPane root;
    @FXML
    CheckBox optimizationCheckbox;
    @FXML
    Button saveButton;
    @FXML
    Button openButton;

    private CudaDriver driver;
    private BaseGpuSimulation simulation;
    private SimulationDisplay gameOfLifeCudaGL;

    private float scroll = 0.0f;
    private float zoom = 1.0f;

    @FXML
    protected void initialize() {
        this.driver = new CudaDriver(0);
        cudaDeviceNameLabel.setText(this.driver.getDeviceName());
        GLProfile profile = GLProfile.get(GLProfile.GL3);
        final GLCapabilities capabilities = new GLCapabilities(profile);
        setAllDisabled(true);
        SwingUtilities.invokeLater(() -> {
            gameOfLifeCudaGL = new SimulationDisplay(swingNode, capabilities);
            setupResetButton();
            setupNextEpochButton();
            setupOpenButton();
            setupCheckbox();
            setupSaveButton();
            setupWorldSizeSlider();
            setupNumOfEpochsSlider();
            setupNumOfThreadsSlider(numOfThreadsSlider);
        });

        root.setOnKeyPressed(event -> {
            KeyCode code = event.getCode();
            System.out.println("Pressed: " + code);
            if (code == KeyCode.ADD) {
                updateZoom(-40.0f);
            } else if (code == KeyCode.SUBTRACT) {
                updateZoom(40.0f);
            } else if (code == KeyCode.LEFT || code == KeyCode.A) {
                updateDisplacement(1, 0);
            } else if (code == KeyCode.RIGHT || code == KeyCode.D) {
                updateDisplacement(-1, 0);
            } else if (code == KeyCode.UP || code == KeyCode.W) {
                updateDisplacement(0, 1);
            } else if (code == KeyCode.DOWN || code == KeyCode.S) {
                updateDisplacement(0, -1);
            }
        });
        root.setOnScroll(event -> {
            float deltaScroll = (float) event.getDeltaY();

            updateZoom(deltaScroll);
        });
    }

    private void setupCheckbox() {
        optimizationCheckbox.setSelected(true);
    }

    private void setupOpenButton() {
        openButton.setOnMouseClicked(event -> {
            Window stage = root.getScene().getWindow();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open saved world");
            File file = fileChooser.showOpenDialog(stage);
            if (file == null) return;
            try {
                boolean isOptimized = optimizationCheckbox.isSelected();
                setNewSimulation(isOptimized ? new OptimizedGameOfLifeSimulation(file.getAbsolutePath(), driver) :
                        new GameOfLifeSimulation(file.getAbsolutePath(), driver));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void setupSaveButton() {
        Window stage = root.getScene().getWindow();

        saveButton.setOnMouseClicked(event -> {
            if (this.simulation == null) return;
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save world");
            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                try {

                    this.simulation.saveOutputToFile(file.getAbsolutePath(), this.driver);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
    }

    private void setupNumOfEpochsSlider() {
        numOfEpochsSlider.setMin(1);
        numOfEpochsSlider.setMax(100);
        numOfEpochsSlider.setBlockIncrement(1);
    }

    private int displacementX = 0;
    private int displacementY = 0;

    private void updateDisplacement(int x, int y) {
        displacementX += 10 * ((float) x / zoom);
        displacementY += 10 * ((float) y / zoom);
        tryDrawSimulation();
    }

    private void updateZoom(float delta) {
        setZoomFromScroll(delta);
        System.out.println("Actual scroll: " + zoom);
        tryDrawSimulation();
    }

    private void tryDrawSimulation() {
        if (gameOfLifeCudaGL != null && simulation != null) {
            drawSimulation(simulation);
        }
    }

    private void setZoomFromScroll(float delta) {
        float normalizedDelta = delta * 0.001f;
        float newScroll = scroll + normalizedDelta;

        if (newScroll >= 1.0f) {
            newScroll = 1.0f;
        } else if (newScroll <= -1.0f) {
            newScroll = -1.0f;
        }

        if (newScroll >= 0.0f) {
            zoom = 1.0f + 4 * newScroll;
        } else {
            zoom = 1 / ((1 + -newScroll * 5.0f));
        }

        scroll = newScroll;
    }

    private void setAllDisabled(boolean value) {
        worldSizeSlider.setDisable(value);
        numOfThreadsSlider.setDisable(value);
        nextEpochButton.setDisable(value);
        worldSizeSlider.setDisable(value);
    }

    private void setupNumOfThreadsSlider(Slider slider) {
        slider.setDisable(false);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        setNumOfThreadsSliderRanges();
    }

    @FXML
    public void shutdown() {
        cleanUp();
    }

    private void setNewSimulation(BaseGpuSimulation simulation) {
        cleanUp();
        this.simulation = simulation;
        setButtonsEnabled();
        setEpochLabelContent();
        if (this.simulation != null) {
            setNumberOfCellsLabelContent();
            drawSimulation(this.simulation);
        }
    }

    private void setNewSimulation(int size) {

        try {
            boolean isOptimizedVersionSelected = optimizationCheckbox.isSelected();
            BaseGpuSimulation newSimulation = isOptimizedVersionSelected ? new OptimizedGameOfLifeSimulation(size) : new GameOfLifeSimulation(size);
            setNewSimulation(newSimulation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setNumOfThreadsSliderRanges() {
        Slider slider = numOfThreadsSlider;
        int step = 32;
        slider.setBlockIncrement(step);
        slider.setMajorTickUnit(step);
        slider.setMinorTickCount(step);
        slider.setMin(step);
        slider.setMax(1024);
        slider.setValue(step * step);
    }

    private void setNumberOfCellsLabelContent() {
        numOfCellsLabel.setText(String.format("Number of cells: %s", NumberFormat.getNumberInstance(Locale.US).format(this.simulation.getNumberOfCells())));
    }

    private void setButtonsEnabled() {
        nextEpochButton.setDisable(this.simulation == null);
    }

    private void cleanUp() {
        if (this.simulation != null) {
            this.simulation.dispose();
        }
    }

    private void setupResetButton() {
        resetButton.setOnMouseClicked(event -> {
            final int size = (int) worldSizeSlider.getValue();
            setAllDisabled(true);
            try {
                setNewSimulation(size);
            } catch (Exception e) {
                e.printStackTrace();
            }
            setAllDisabled(false);
        });
    }

    private void setupNextEpochButton() {
        setButtonsEnabled();
        nextEpochButton.setOnMouseClicked(event -> {
            if (simulation != null) {
                boolean result = simulation.runNextEpoch((int) numOfEpochsSlider.getValue(),  (int) numOfThreadsSlider.getValue());
                setEpochLabelContent();
                if (result) {
                    drawSimulation(simulation);
                }
            }
        });
    }

    private void drawSimulation(BaseGpuSimulation simulation) {
        System.out.printf("Drawing sim with zoom =%s dx,dy =%d,%d%n", zoom, displacementX, displacementY);
        gameOfLifeCudaGL.drawSimulation(simulation, zoom, displacementX, displacementY);
    }

    private void setEpochLabelContent() {
        if (simulation == null) return;
        StringBuilder messageBuilder = new StringBuilder();
        int epoch = simulation.getEpoch();
        messageBuilder.append(String.format("Epoch: %d", simulation.getEpoch()));
        if (epoch > 0) {
            messageBuilder.append(String.format("\nLast run took %s.", simulation.getLastRunDuration()));
        } else {
            messageBuilder.append("\n-");
        }
        epochLabel.setText(messageBuilder.toString());
    }

    private static final int MIN_WORLD_SIZE_AND_INCREMENT = 10240;

    private static final int MAX_MULTIPLE_OF_MIN_WORLD_SIZE = 4;

    private void setupWorldSizeSlider() {
        worldSizeSlider.setDisable(false);
        worldSizeSlider.setMin(MIN_WORLD_SIZE_AND_INCREMENT);
        worldSizeSlider.setMax(MAX_MULTIPLE_OF_MIN_WORLD_SIZE * MIN_WORLD_SIZE_AND_INCREMENT);
        worldSizeSlider.setBlockIncrement(MIN_WORLD_SIZE_AND_INCREMENT);
        worldSizeSlider.setShowTickLabels(true);
        worldSizeSlider.setShowTickMarks(true);
        worldSizeSlider.setMajorTickUnit(MIN_WORLD_SIZE_AND_INCREMENT);
        worldSizeSlider.setMinorTickCount(MIN_WORLD_SIZE_AND_INCREMENT);
    }
}

