package gui;
/**
 * Created by Kamil on 13.05.2017.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class GameOfLifeGUI extends Application {

    public static void main(String[] args) {
            launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        URL resourceUrl = getClass().getResource("/mainwindow.fxml");
        GridPane root = FXMLLoader.load(resourceUrl);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setMinWidth(1280);
        primaryStage.setMinHeight(1024);
        primaryStage.setTitle("CUDA Game of life");
        primaryStage.show();
    }
}
