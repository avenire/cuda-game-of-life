package console;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Kamil on 31.05.2017.
 */
public class RunnerParamsParser {
    private String inputFile;
    private String outputFile;
    private int numberOfThreads;
    private long numberOfCells;
    private int numberOfEpochs;
    private int argIndex = 0;
    private int repetitions = 1;
    private int deviceNumber = 0;
    private boolean isOptimized = true;
    private String[] args;
    private boolean isReporterEnabled = true;

    public RunnerParams parse(String[] args) {
        this.args = args;
        resetToDefaults();

        while(argIndex < args.length) {
                parseArgument();
        }

        return new RunnerParams(inputFile, outputFile, numberOfThreads, numberOfCells, numberOfEpochs, repetitions, deviceNumber, isOptimized);
    }

    private void parseArgument() {
        String arg = args[argIndex++];

        if(arg.equals("-b")) {
            parseNumberOfThreads();
        } else if(arg.equals("-e")) {
            parseNumberOfEpochs();
        } else if(arg.equals("-n")) {
            parseNumberOfCells();
        } else if(arg.equals("-i")) {
            parseInputFile();
        } else if(arg.equals("-o")) {
            parseOutputFile();
        } else if(arg.equals("-d")) {
            parseDeviceNumber();
        } else if(arg.equals("--no-optimizations")) {
            setIsOptimized();
        } else if(arg.equals("-r")) {
            parseRepetitions();
        } else if(arg.equals("-h")) {
            printHelp();
        } else if(arg.equals("--no-report")) {
            setReporterDisabled();
        }
    }

    private void setReporterDisabled() {
        this.isReporterEnabled = false;
    }

    private void setIsOptimized() {
        this.isOptimized = false;
    }

    private void parseDeviceNumber() {
        this.deviceNumber = (int)parseNumber();
    }

    private void parseRepetitions() {
        repetitions = (int)Math.max(1, parseNumber());
    }

    private void printHelp() {
        argIndex = args.length;

        System.out.print("CUDA Game Of Life options\n" +
                "-h:\t\tHelp." +
                "\n-n <Long>\tSet total number of cells." +
                "\n-e <Int>\tSet number of epochs to simulate" +
                "\n-b <Int>\tSet number of CUDA threads." +
                "\n-i <Path>\tLoad world from file instead of generating random one." +
                "\n-o <Path>\tSave simulated world to file." +
                "\n-r <Int>\tNumber of run repetitions." +
                "\n-d <Int>\tCUDA device id." +
                "\n--no-optimizations\tDisabled optimized version." +
                "\n--no-report\tDisable CSV report and enabled default, lame prints." +
                "\n");
    }

    private void parseOutputFile() {
        try {
            outputFile = parseFilePath();
        } catch (IOException e) {
            e.printStackTrace();
            outputFile = "";
        }
    }

    private String parseFilePath() throws IOException {
        Path filePath = Paths.get(args[argIndex++]);
        ensurePathExists(filePath);
        return filePath.toString();
    }

    private void ensurePathExists(Path filePath) throws IOException {
        if(Files.notExists(filePath)) {
            Files.createFile(filePath);
        }
    }

    private void parseInputFile() {
        try {
            inputFile = parseFilePath();
        } catch (IOException e) {
            e.printStackTrace();
            inputFile = "";
        }
    }

    private void parseNumberOfCells() {
        numberOfCells = parseNumber();
    }

    private long parseNumber() {
        String numberString = args[argIndex++];
        return Long.parseLong(numberString);
    }

    private void parseNumberOfEpochs() {
        numberOfEpochs = (int) parseNumber();
    }

    private void parseNumberOfThreads() {
        numberOfThreads = (int) parseNumber();
    }

    private void resetToDefaults() {
        inputFile = "";
        outputFile = "";
        numberOfThreads = 128;
        numberOfCells = 1600000;
        numberOfEpochs = 100;
        argIndex = 0;
    }
}
