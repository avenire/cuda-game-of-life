package console;

import cuda.*;

import java.io.IOException;

public class Runner {
    private final RunnerParams params;
    private CudaDriver driver;
    private ICustomReporter reporter;
    private BaseSimulationFactory simulationFactory;

    public Runner(ICustomReporter reporter, RunnerParams params) {
        this.reporter = reporter;
        this.params = params;
        this.simulationFactory = getFactory(params.isOptimized());
        this.driver = new CudaDriver(params.getDeviceNumber());
    }

    public void runSimulation() {
        try {
            runSimulationInternal();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printRunSimulationConfig(RunnerParams params) {
        System.out.print(String.format("CUDA device name %s (%d) %nSimulating %d cells for %d epochs.%nCUDA threads: %d%nInput file: %s%nOutput file: %s%n",
                driver.getDeviceName(),
                params.getDeviceNumber(),
                params.getNumberOfCells(),
                params.getNumberOfEpochs(),
                params.getNumberOfThreads(),
                params.getInputFile(),
                params.getOutputFile()));
    }

    private void runSimulationInternal() throws IOException {
        BaseGpuSimulation simulation = createSimulation(params);
        int repetitions = params.getRepetitions();
        boolean isSuccessful = false;

        while (repetitions-- > 0) {
            isSuccessful = simulation.runNextEpoch(params.getNumberOfEpochs(), params.getNumberOfThreads());
            System.out.println(reporter.createReport(params, new RunnerExecutionStatus(driver.getDeviceName(), simulation.getLastRunDuration())));
        }
        if (isSuccessful) {
            saveWorld(params, simulation);
        }
        simulation.dispose();
    }

    private BaseSimulationFactory getFactory(boolean optimized) {
        return optimized ? new OptimizedSimulationFactory(driver) : new NonOptimizedSimulationFactory(driver);
    }

    private void saveWorld(RunnerParams params, BaseGpuSimulation simulation) {
        if (params.hasOutputFile()) {
            simulation.saveOutputToFile(params.getOutputFile(), driver);
        }
    }

    private BaseGpuSimulation createSimulation(RunnerParams params) throws IOException {
        if (params.hasInputFile()) {
            return simulationFactory.createFromFile(params.getInputFile());
        } else {
            int cellsInRow = (int) Math.ceil(Math.sqrt(params.getNumberOfCells()));
            return simulationFactory.createWithRandomData(cellsInRow);
        }
    }
}
