package console;

/**
 * Created by Kamil on 31.05.2017.
 */
public class RunnerParams {

    private String inputFile;
    private String outputFile;
    private int numberOfThreads;
    private long numberOfCells;
    private int numberOfEpochs;
    private int repetitions;
    private int deviceNumber;
    private boolean isOptimized;

    public RunnerParams(String inputFile, String outputFile, int numberOfThreads, long numberOfCells, int numberOfEpochs, int repetitions, int deviceNumber, boolean isOptimized) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        this.numberOfThreads = numberOfThreads;
        this.numberOfCells = numberOfCells;
        this.numberOfEpochs = numberOfEpochs;
        this.repetitions = repetitions;
        this.deviceNumber = deviceNumber;
        this.isOptimized = isOptimized;
    }

    public long getNumberOfCells() {
        return numberOfCells;
    }

    public boolean hasInputFile() {return inputFile != null && !inputFile.isEmpty();}

    public boolean hasOutputFile() {return outputFile != null && !outputFile.isEmpty();}

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public String getInputFile() {
        return inputFile;
    }

    public int getNumberOfEpochs() {
        return numberOfEpochs;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public int getDeviceNumber() {
        return deviceNumber;
    }

    public boolean isOptimized() {
        return isOptimized;
    }
}
