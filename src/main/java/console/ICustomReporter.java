package console;

public interface ICustomReporter {
    public String createReport(RunnerParams params, RunnerExecutionStatus status);
}

