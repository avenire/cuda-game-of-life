package console;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class CsvReporter implements ICustomReporter {

    @Override
    public String createReport(RunnerParams params, RunnerExecutionStatus status) {
        LocalTime t = LocalTime.MIDNIGHT.plus(status.getExecutionDuration());
        String durationPretty = DateTimeFormatter.ofPattern("HH:mm:ss.SSS").format(t);
        return String.format("%s,%d,%d,%s,%s", status.getDeviceName(), params.getNumberOfThreads(), params.getNumberOfCells(), durationPretty, params.isOptimized());
    }
}
