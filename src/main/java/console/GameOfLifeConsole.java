package console;

/**
 * Created by Kamil on 12.05.2017.
 */
public class GameOfLifeConsole {
    public static void main(String[] args) {
            new Runner(new CsvReporter(), new RunnerParamsParser().parse(args)).runSimulation();
    }
}
