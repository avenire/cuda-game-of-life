package console;

import java.time.Duration;
import java.time.Instant;

public class RunnerExecutionStatus {
    private String deviceName;
    private Duration executionDuration;

    public RunnerExecutionStatus(String deviceName, Duration executionDuration) {
        this.deviceName = deviceName;
        this.executionDuration = executionDuration;
    }

    public Duration getExecutionDuration() {
        return executionDuration;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
