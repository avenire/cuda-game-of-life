package cuda;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;
import jcuda.nvrtc.nvrtcProgram;

import java.io.*;
import java.util.stream.Collectors;

import static jcuda.driver.JCudaDriver.*;
import static jcuda.nvrtc.JNvrtc.*;

/**
 * Created by Kamil on 12.05.2017.
 */
public class CudaDriver {

    private CUdevice device;

    public CudaDriver(int deviceNumber) {
        initializeCuda(deviceNumber);
    }

    public static CUmodule getCudaModuleFromResourceFile(String resourceFilename) throws IOException {
        InputStream stream = null;
        try {
            CUmodule module = new CUmodule();
            byte[] kernelFilePath;
            stream = CudaDriver.class.getResourceAsStream(resourceFilename);

            kernelFilePath = toZeroTerminatedStringByteArray(stream);
            stream.close();
            cuModuleLoadData(module, kernelFilePath);
            return module;
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

    private static byte[] toZeroTerminatedStringByteArray(
            InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte buffer[] = new byte[8192];
        while (true) {
            int read = inputStream.read(buffer);
            if (read == -1) {
                break;
            }
            baos.write(buffer, 0, read);
        }
        baos.write(0);
        return baos.toByteArray();
    }

    public void initializeCuda(int id) {
        JCudaDriver.setExceptionsEnabled(true);
        cuInit(0);
        createDevice(id);
        createContext();
    }

    private void createDevice(int id) {
        this.device = new CUdevice();
        cuDeviceGet(device, id);
    }

    private void createContext() {
        CUcontext context = new CUcontext();
        cuCtxCreate(context, 0, device);
    }

    public String getDeviceName() {
        byte deviceName[] = new byte[1024];
        cuDeviceGetName(
                deviceName, deviceName.length, device);
        return createString(deviceName);
    }

    public static CUfunction getCudaKernelFromPtxResource(String moduleName, String kernelName) throws IOException {
        CUmodule module = getCudaModuleFromResourceFile(moduleName);
        CUfunction function = new CUfunction();
        cuModuleGetFunction(function, module, kernelName);
        return function;
    }

    public static void runKernel(CUfunction function, int gridSizeX, int gridSizeY, int blockSizeX, int blockSizeY, Pointer params) {
        cuLaunchKernel(function,
                gridSizeX, gridSizeY, 1,      // Grid dimension
                blockSizeX, blockSizeY, 1,      // Block dimension
                0, null,               // Shared memory size and stream
                params, null // Kernel- and extra parameters
        );
        cuCtxSynchronize();
    }

    private static String createString(byte bytes[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            char c = (char) bytes[i];
            if (c == 0) {
                break;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static CUfunction getCudaKernelFromCuResource(String resourcePath, String functionName, String params, boolean isQuiet) {
        try {
            CUmodule module = new CUmodule();
            String sourceCode = getKernelSourceCode(resourcePath);

            String[] ptx = compilePtx(params, sourceCode, isQuiet);
            return getFunctionFromPtx(functionName, module, ptx[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static CUfunction getFunctionFromPtx(String functionName, CUmodule module, String ptx) {
        cuModuleLoadData(module, ptx);
        CUfunction function = new CUfunction();
        cuModuleGetFunction(function, module, functionName);
        return function;
    }

    private static String[] compilePtx(String params, String sourceCode, boolean isQuiet) {
        nvrtcProgram program = new nvrtcProgram();
        nvrtcCreateProgram(
                program, sourceCode, null, 0, null, null);
        nvrtcCompileProgram(program, 1, new String[]{params});
        String programLog[] = new String[1];
        nvrtcGetProgramLog(program, programLog);
        if (!isQuiet) {
            System.out.println("Program compilation log:\n" + programLog[0]);
        }
        String[] ptx = new String[1];
        nvrtcGetPTX(program, ptx);
        nvrtcDestroyProgram(program);
        return ptx;
    }

    private static String getKernelSourceCode(String resourcePath) throws IOException {
        InputStream stream = null;
        String sourceCode = "";
        try {
            stream = CudaDriver.class.getResourceAsStream(resourcePath);
            sourceCode = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));

        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return sourceCode;
    }

    public void copyToDevice(byte[] data, CUdeviceptr cellInputPointer) {
        cuMemcpyHtoD(cellInputPointer, Pointer.to(data), Sizeof.BYTE * data.length);
    }

    public byte[] copyFromDevice(CUdeviceptr cellInputPointer, long numOfBytes) {
        byte[] bytes = new byte[(int) numOfBytes];
        cuMemcpyDtoH(Pointer.to(bytes), cellInputPointer, Sizeof.BYTE * numOfBytes);
        return bytes;
    }
}
