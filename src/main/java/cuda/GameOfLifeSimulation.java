package cuda;

import jcuda.driver.CUfunction;

import java.io.IOException;

import static jcuda.driver.JCudaDriver.cuCtxSynchronize;
import static jcuda.driver.JCudaDriver.cuLaunchKernel;
import jcuda.*;
/**
 * Created by Kamil on 12.05.2017.
 */
public class GameOfLifeSimulation extends BaseGpuSimulation {
    private CUfunction drawKernel;

    private CUfunction computeNextStateKernel;

    public GameOfLifeSimulation(String absolutePath, CudaDriver driver) throws IOException {
        super(absolutePath, driver);
    }

    @Override
    public long getNumberOfCells() {
        return world.getNumberOfBytesUsed();
    }

    @Override
    protected void initWorldWithRandomData(GameOfLifeWorldParameters world, Pointer paramsPointer) {
        try {
            CUfunction kernel = CudaDriver.getCudaKernelFromPtxResource("/randinit.ptx", "randinit");
            int blockSize = 32;
            int gridSizeY = (int) Math.ceil((double)world.getSize() / blockSize);
            int gridSizeX = (int) Math.ceil((double)world.getSize() / blockSize);

            CudaDriver.runKernel(kernel, gridSizeX, gridSizeY, blockSize, blockSize, paramsPointer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected CUfunction getKernel(int numberOfThreads) {
        if(this.computeNextStateKernel == null) {
            try {
                this.computeNextStateKernel = CudaDriver.getCudaKernelFromPtxResource("/simkernel.ptx", "computeNextState");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.computeNextStateKernel;
    }

    @Override
    protected GpuPointers createGpuPointers(GameOfLifeWorldParameters params) {
        return new GpuPointers(world);
    }

    public GameOfLifeSimulation(int size) throws IOException {
        super(size);
        initWorldWithRandomData(world, gpuPointers.getInitDataKernelPointers());
    }

    @Override
    public CUfunction getDrawKernel() {
        if(this.drawKernel == null) {
            try {
                this.drawKernel = CudaDriver.getCudaKernelFromPtxResource("/glkernel.ptx", "draw");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.drawKernel;
    }

    protected void runChangeStateKernel(CUfunction function, GpuPointers kernelBuffers, int numberOfThreads) {
        int blockSize = (int)Math.ceil(Math.sqrt(numberOfThreads));
        int gridSizeX = (int) Math.ceil((double)world.getSize() / blockSize);
        int gridSizeY = (int) Math.ceil((double)world.getSize() / blockSize);
        cuLaunchKernel(function,
                gridSizeX, gridSizeY, 1,      // Grid dimension
                blockSize, blockSize, 1,      // Block dimension
                0, null,               // Shared memory size and stream
                kernelBuffers.getSimulationKernelPointers(), null // Kernel- and extra parameters
        );
        cuCtxSynchronize();
    }
}
