package cuda;

import java.io.IOException;

/**
 * Created by Kamil on 03.06.2017.
 */
public abstract class BaseSimulationFactory {

    protected CudaDriver driver;

    public BaseSimulationFactory(CudaDriver driver) {
        this.driver = driver;
    }

    public abstract BaseGpuSimulation createFromFile(String filepath) throws IOException;
    public abstract BaseGpuSimulation createWithRandomData(int numberOfCellsInRow) throws IOException;
}

