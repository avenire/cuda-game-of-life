package cuda;

import jcuda.Pointer;
import jcuda.driver.CUfunction;

import java.io.IOException;

import static jcuda.driver.JCudaDriver.cuCtxSynchronize;
import static jcuda.driver.JCudaDriver.cuLaunchKernel;

/**
 * Created by Kamil on 13.05.2017.
 */

public class OptimizedGameOfLifeSimulation extends BaseGpuSimulation {

    private CUfunction drawKernel;

    public OptimizedGameOfLifeSimulation(int size) throws IOException {
        super(size);
    }

    public OptimizedGameOfLifeSimulation(String absolutePath, CudaDriver driver) throws IOException {
        super(absolutePath, driver);
    }


    @Override
    public CUfunction getDrawKernel() {
        if(this.drawKernel == null) {
            try {
                this.drawKernel = CudaDriver.getCudaKernelFromPtxResource("/glkernel_optimized.ptx", "draw");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.drawKernel;
    }

    private CUfunction cachedKernel = null;
    int cachedKernelDefinedBlockSize = 0;
    private CUfunction getOptimizedKernel(int blockSize) {
        if(cachedKernelDefinedBlockSize != blockSize) {
            this.cachedKernel = CudaDriver.getCudaKernelFromCuResource("/simkernel_optimized.cu", "computeNextState", "-DBLOCK_SIZE=" + blockSize, true);
            cachedKernelDefinedBlockSize = blockSize;
        }

        return this.cachedKernel;
    }

    @Override
    public long getNumberOfCells() {
        return world.getNumberOfBytesUsed() * 8;
    }

    @Override
    protected void initWorldWithRandomData(GameOfLifeWorldParameters world, Pointer paramsPointer) {
        try {
            CUfunction kernel = CudaDriver.getCudaKernelFromPtxResource("/randinit_optimized.ptx", "randinit");
            int blockSize = 32;
            int gridSizeY = (int) Math.ceil((double)world.getSize() / blockSize);
            int gridSizeX = (int) Math.ceil((double)world.getSize() / blockSize);

            CudaDriver.runKernel(kernel, gridSizeX, gridSizeY, blockSize, blockSize, paramsPointer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected CUfunction getKernel(int numberOfThreads) {
        return getOptimizedKernel(numberOfThreads);
    }

    @Override
    protected GpuPointers createGpuPointers(GameOfLifeWorldParameters params) {
        return new GpuPointers(world);
    }

    @Override
    protected void runChangeStateKernel(CUfunction function, GpuPointers kernelBuffers, int numberOfThreads) {

        int gridSizeX = (int) Math.ceil((double)world.getSize() / numberOfThreads);
        // Call the kernel computeNextStateKernel.
        cuLaunchKernel(function,
                gridSizeX, 1, 1,      // Grid dimension
                numberOfThreads, 1, 1,      // Block dimension
                0, null,               // Shared memory size and stream
                kernelBuffers.getSimulationKernelPointers(), null // Kernel- and extra parameters
        );
        cuCtxSynchronize();
    }
}
