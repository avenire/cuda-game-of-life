package cuda;

/**
 * Created by Kamil on 12.05.2017.
 */
public class GameOfLifeWorldParameters {

    private final double chance;

    public long getSize() {
        return size;
    }

    private long size;

    public GameOfLifeWorldParameters(long size) {
        this(size, 0.25);
    }

    public GameOfLifeWorldParameters(long size, double chanceToSpawnAliveCell) {
        this.size = size;
        this.chance = chanceToSpawnAliveCell;
    }

    public long getNumberOfBytesUsed() {return size * size;}

    public double getChance() {
        return chance;
    }
}
