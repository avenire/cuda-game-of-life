package cuda;

import java.io.IOException;

public class OptimizedSimulationFactory extends BaseSimulationFactory {

    public OptimizedSimulationFactory(CudaDriver driver) {
        super(driver);
    }

    @Override
    public BaseGpuSimulation createFromFile(String filepath) throws IOException {
        return new OptimizedGameOfLifeSimulation(filepath, this.driver);
    }

    @Override
    public BaseGpuSimulation createWithRandomData(int numberOfCellsInRow) throws IOException {
        // todo: refactor hack
        return new OptimizedGameOfLifeSimulation((int)Math.ceil(numberOfCellsInRow / 8.0));
    }
}
