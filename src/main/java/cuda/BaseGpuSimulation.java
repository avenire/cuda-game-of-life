package cuda;

import jcuda.CudaException;
import jcuda.Pointer;
import jcuda.driver.CUfunction;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.Instant;
/**
 * Created by Kamil on 12.05.2017.
 */
public abstract class BaseGpuSimulation {

    protected final GameOfLifeWorldParameters world;
    protected final GpuPointers gpuPointers;
    private Duration lastRunDuration;

    public GpuPointers getGpuPointers() {
        return gpuPointers;
    }

    public int getEpoch() {
        return epoch;
    }

    private int epoch = 0;

    public boolean runNextEpoch(int numOfIncrements, int numberOfThreads) {

        int epochsJump = Math.max(1, numOfIncrements);
        Instant start = Instant.now();
        while (epochsJump-- > 0) {
            try {
                runChangeStateKernel(getKernel(numberOfThreads), gpuPointers, numberOfThreads);
            } catch (CudaException e) {
                logKernelException(e);
                e.printStackTrace();
                return false;
            }
            ++epoch;
            gpuPointers.swapInOutCellsPointers();
        }

        Instant end = Instant.now();
        lastRunDuration = Duration.between(start, end);

        return true;
    }

    private void logKernelException(CudaException e) {
        try {
            String fileName = String.format("kernel_crash_%d", System.nanoTime());
            PrintWriter pw = new PrintWriter(new File(fileName));
            e.printStackTrace(pw);
            pw.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public abstract long getNumberOfCells();

    protected abstract void initWorldWithRandomData(GameOfLifeWorldParameters world, Pointer paramsPointer);

    protected abstract CUfunction getKernel(int numberOfThreads);

    protected abstract GpuPointers createGpuPointers(GameOfLifeWorldParameters params);

    public BaseGpuSimulation(int bytesPerRowAndColumn) throws IOException {
        this.size = bytesPerRowAndColumn;
        this.world = new GameOfLifeWorldParameters(bytesPerRowAndColumn);
        this.gpuPointers = createGpuPointers(world);
        initWorldWithRandomData(world, gpuPointers.getInitDataKernelPointers());
    }


    public BaseGpuSimulation(String file, CudaDriver driver) throws IOException {
        Path path = Paths.get(file);
        byte[] data = Files.readAllBytes(path);
        this.size = (int)Math.sqrt(data.length);
        this.world = new GameOfLifeWorldParameters(size);
        this.gpuPointers = createGpuPointers(world);
        driver.copyToDevice(data, this.gpuPointers.getCellInputPointer());
    }

    public void saveOutputToFile(String file, CudaDriver driver) {
        byte[] data = driver.copyFromDevice(this.gpuPointers.getCellInputPointer(), world.getNumberOfBytesUsed());
        try {
            Path path = Paths.get(file);
            ensureFileExists(path);
            Files.write(path, data, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ensureFileExists(Path path) throws IOException {
        if (!Files.exists(path)) {
            Files.createFile(path);
        }
    }

    private int size;

    public int getSize() {
        return size;
    }

    public void dispose() {
        gpuPointers.dispose();
    }

    public abstract CUfunction getDrawKernel();

    protected abstract void runChangeStateKernel(CUfunction function, GpuPointers kernelBuffers, int numberOfThreads);

    public Duration getLastRunDuration() {return lastRunDuration;}
}
