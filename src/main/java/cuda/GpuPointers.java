package cuda;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUdeviceptr;

import static jcuda.driver.JCudaDriver.*;
import static jcuda.driver.JCudaDriver.cuMemFree;

public class GpuPointers {
    private final GameOfLifeWorldParameters worldParams;

    public Pointer getSimulationKernelPointers() {

        return  Pointer.to(
                Pointer.to(new long[]{worldParams.getSize()}),
                Pointer.to(cellInputPointer),
                Pointer.to(cellOutputPointer)
        );
    }

    public Pointer getInitDataKernelPointers() {
        return  Pointer.to(
                Pointer.to(new long[]{worldParams.getSize()}),
                Pointer.to(cellInputPointer),
                Pointer.to(new double[]{worldParams.getChance()})
        );
    }

    private CUdeviceptr cellOutputPointer;

    public CUdeviceptr getCellInputPointer() {
        return cellInputPointer;
    }

    private CUdeviceptr cellInputPointer;

    public void swapInOutCellsPointers() {
        CUdeviceptr pointer = cellInputPointer;
        cellInputPointer = cellOutputPointer;
        cellOutputPointer = pointer;
    }

    public void dispose() {
        cuMemFree(cellInputPointer);
        cuMemFree(cellOutputPointer);
    }

    public GpuPointers(GameOfLifeWorldParameters worldParams) {
        this.worldParams = worldParams;
        long numberOfBytesUsed = worldParams.getNumberOfBytesUsed();

        cellInputPointer = new CUdeviceptr();
        cuMemAlloc(cellInputPointer, getSizeInBytes(numberOfBytesUsed));

        cellOutputPointer = new CUdeviceptr();
        cuMemAlloc(cellOutputPointer, getSizeInBytes(numberOfBytesUsed));
    }

    public long getSizeInBytes(long numberOfBytes) {
        return numberOfBytes * Sizeof.BYTE;
    }

}
