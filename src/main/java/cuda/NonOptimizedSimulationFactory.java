package cuda;

import java.io.IOException;

public class NonOptimizedSimulationFactory extends BaseSimulationFactory {

    public NonOptimizedSimulationFactory(CudaDriver driver) {
        super(driver);
    }

    @Override
    public BaseGpuSimulation createFromFile(String filepath) throws IOException {
        return new GameOfLifeSimulation(filepath, driver);
    }

    @Override
    public BaseGpuSimulation createWithRandomData(int numberOfCellsInRow) throws IOException {
        return new GameOfLifeSimulation(numberOfCellsInRow);
    }
}
