extern "C"
 __global__ void computeNextState(int worldSide, unsigned char* cells, unsigned char* newCells)
 {
	const int NUM_OF_COLUMNS =  (BLOCK_SIZE + 2);
    const int CACHE_SIZE = NUM_OF_COLUMNS * 3;
    unsigned long  worldSize = (worldSide * worldSide);
    unsigned int width = worldSide;
    unsigned int height = worldSide;
    __shared__ unsigned char surroundings[CACHE_SIZE];
    int increment = BLOCK_SIZE * gridDim.x;
    unsigned long cellsIndex = ((blockIdx.x * BLOCK_SIZE) + threadIdx.x);
    unsigned long startingCellId = (blockIdx.x * BLOCK_SIZE);
    unsigned int iterations = ceil((double)(worldSize - startingCellId) / increment);

    int i = 0;
    for (;i++ < iterations; cellsIndex += increment) {

      unsigned int index = cellsIndex % worldSize;
      unsigned int  x = (cellsIndex + worldSide - 1) % worldSide;
      unsigned int  y = ((index / worldSide) * worldSide);
      unsigned int  top_y = (y + worldSize - worldSide) % worldSize;
      unsigned int  bottom_y = (y + worldSide) % worldSize;
	  unsigned int sharedMemoryOffset = threadIdx.x;

      __syncthreads();
      do {

          surroundings[sharedMemoryOffset] = cells[x + top_y];
          surroundings[sharedMemoryOffset + NUM_OF_COLUMNS] = cells[x + y];
          surroundings[sharedMemoryOffset + 2 * NUM_OF_COLUMNS] = cells[x + bottom_y];

          x += BLOCK_SIZE;
          x %= worldSide;
          sharedMemoryOffset += BLOCK_SIZE;
      } while(sharedMemoryOffset < NUM_OF_COLUMNS);
      __syncthreads();
       if(cellsIndex >= worldSize) { continue; }

		unsigned int index_in_surroundings = threadIdx.x;
		unsigned int topRow =    (unsigned int)surroundings[index_in_surroundings] << 16;
		unsigned int middleRow = (unsigned int)surroundings[index_in_surroundings + NUM_OF_COLUMNS] << 16;
		unsigned int bottomRow = (unsigned int)surroundings[index_in_surroundings + 2 * NUM_OF_COLUMNS] << 16;

		index_in_surroundings += 1;
		topRow    |= (unsigned int)surroundings[index_in_surroundings] << 8;
		middleRow |= (unsigned int)surroundings[index_in_surroundings + NUM_OF_COLUMNS] << 8;
		bottomRow |= (unsigned int)surroundings[index_in_surroundings + 2 * NUM_OF_COLUMNS] << 8;

		index_in_surroundings += 1;
		topRow    |= (unsigned int)surroundings[index_in_surroundings];
		middleRow |= (unsigned int)surroundings[index_in_surroundings + NUM_OF_COLUMNS];
		bottomRow |= (unsigned int)surroundings[index_in_surroundings + 2 * NUM_OF_COLUMNS];

		unsigned int result = 0;
		for (unsigned int j = 0; j < 8; ++j) {

            unsigned int aliveCells = ((topRow & 0x14000) + (middleRow & 0x14000) + (bottomRow & 0x14000)) >> 14;
            aliveCells = (aliveCells & 0x3) + (aliveCells >> 2) + ((topRow >> 15) & 0x1) + ((bottomRow >> 15) & 0x1);
            result = result << 1 | (((((middleRow & 0x8000) >> 15) | aliveCells) == 3) ? 0x1 : 0x0);

            topRow <<= 1;
            middleRow <<= 1;
            bottomRow <<= 1;
		}

		newCells[cellsIndex] = result;
      }
}

/*
            unsigned int aliveCells = ((topRow & 0x14000u) + (middleRow & 0x14000u) + (bottomRow & 0x14000u)) >> 14;
            aliveCells = (aliveCells & 0x3u) + (aliveCells >> 2) + ((topRow >> 15) & 0x1u) + ((bottomRow >> 15) & 0x1u);
            result = result << 1 |
                     (((((middleRow & 0x8000u) >> 15) | aliveCells) == 3) ? 0x1u : 0x0u);
*/

/*
aliveCells >>= 14;
        aliveCells = (aliveCells & 0x3) + (aliveCells >> 2)
          + ((topRow >> 15) & 0x1u) + ((bottomRow >> 15) & 0x1u);

        result = result << 1 | (aliveCells == 3 || (aliveCells == 2 && (middleRow & 0x8000u)) ? 1u : 0u);
*/