package console;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Kamil on 31.05.2017.
 */
public class RunnerParamsParserTest {
    @Test
    public void parsingEmptyArgumentsShouldReturnDefaultOnes() {
        RunnerParamsParser sut = new RunnerParamsParser();

        RunnerParams params = sut.parse(new String[0]);

        assertEquals(128, params.getNumberOfThreads());
        assertEquals(1600000, params.getNumberOfCells());
        assertEquals(100, params.getNumberOfEpochs());
        assertEquals("", params.getInputFile());
        assertEquals("", params.getOutputFile());
    }

    @Test
    public void parsingWithNumOfThreadsAndNumOfCellsAndEpochsShouldReturnParamsWithProperValues() {
        RunnerParamsParser sut = new RunnerParamsParser();

        RunnerParams params = sut.parse(new String[]{"-n", "400000", "-e", "200", "-b", "1024"});

        assertEquals(1024, params.getNumberOfThreads());
        assertEquals(400000, params.getNumberOfCells());
        assertEquals(200, params.getNumberOfEpochs());
        assertEquals("", params.getInputFile());
        assertEquals("", params.getOutputFile());
    }
}