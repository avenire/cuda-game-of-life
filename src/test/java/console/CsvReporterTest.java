package console;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;

/**
 * Created by Kamil on 02.06.2017.
 */
public class CsvReporterTest {
    @Test
    public void checkStringInCorrectFormat() {
        CsvReporter sut = new CsvReporter();
        RunnerParams params = new RunnerParams("", "", 128, 10000, 100, 1, 0, true);
        RunnerExecutionStatus status = new RunnerExecutionStatus("Test C2050", Duration.ofSeconds(2));
        String actual = sut.createReport(params, status);

        Assert.assertEquals("Test C2050,128,10000,00:00:02.000,true", actual);
    }
}
