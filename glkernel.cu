 __device__ int getIndexForCellAt(int x, int y, int n) {
    return x + n * y;
 }

extern "C"
__global__ void draw(uchar4* pos, unsigned int width, unsigned int height, int n, float zoomFactor, int d_x, int d_y, unsigned char* cells) {

    unsigned int px = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int py = blockIdx.y * blockDim.y + threadIdx.y;
    int2 displacement;
    displacement.x = d_x;
    displacement.y = d_y;

    int pixelId = px + py * width;

    int x = (int)floor(((int)(pixelId % width) - displacement.x) * zoomFactor);
    int y = (int)floor(((int)(pixelId / height) - displacement.y) * zoomFactor);
    x = ((x % (int)n) + n) % n;
    y = ((y % (int)n) + n) % n;

    unsigned char value = cells[x + y * n] * 255;

    bool isNotOnBoundary = !(x == 0 || y == 0);

    pos[pixelId].z = value;
    pos[pixelId].x = isNotOnBoundary ? value : 255;
    pos[pixelId].y = value;
}
