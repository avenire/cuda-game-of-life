 __device__ int getIndexForCellAt(int x, int y, int n) {
    return x + n * y;
 }

 __device__ int getScore(int x, int y, int n, unsigned char* cells) {
    int sum = 0;
    int n_x, n_y, d_x, d_y; // coords of neighbour, delta - neighbour relative to cell x,y
    for(d_x = -1; d_x < 2; ++d_x) {
        for(d_y = -1; d_y < 2; ++d_y) {
            if(d_x == 0 && d_y == 0) {
                continue;
            }
            n_x = x + d_x;
            n_y = y + d_y;
            bool is_x_withinBounds = n_x >= 0 && n_x < n;
            bool is_y_withinBounds = n_y >= 0 && n_y < n;
            if(is_x_withinBounds && is_y_withinBounds) {
                sum += cells[getIndexForCellAt(n_x, n_y, n)];
            }
        }
    }
    return sum;
 }

extern "C"
 __global__ void computeNextState(int n, unsigned char* cells, unsigned char* newCells)
 {
     int x = blockIdx.x * blockDim.x + threadIdx.x;
     int y = blockIdx.y * blockDim.y + threadIdx.y;

     if (x<n && y<n)
     {
        int indexOfCell = getIndexForCellAt(x, y, n);
        int score = getScore(x, y, n, cells);
        int currentState = cells[indexOfCell];
        int newState = currentState;
        bool isCellDeadButHasEnoughNeighboursToBecomeAlive = score == 3 && currentState == 0;
        bool isCellAliveAndShouldStayAlive = currentState == 1 && score >= 2 && score <= 3;

        if(isCellDeadButHasEnoughNeighboursToBecomeAlive || isCellAliveAndShouldStayAlive ) {
            newState = 1;
        } else {
            newState = 0;
        }
        newCells[indexOfCell] = newState;
     }
 }

