#include <stdio.h>
#include <curand.h>
#include <curand_kernel.h>
#include <time.h>

extern "C"
__global__ void randinit(long n, unsigned char* cells, double chance) {

    long x = blockIdx.x * blockDim.x + threadIdx.x;
    long y = blockIdx.y * blockDim.y + threadIdx.y;

    curandState state;
    curand_init((unsigned long long)clock() + x + y, 0, 0, &state);
    if(x < n && y < n) {
        double chanceRoll = (double)curand_uniform(&state);
        cells[x + y * n] = chanceRoll < chance ? 1 : 0;
    }
}

