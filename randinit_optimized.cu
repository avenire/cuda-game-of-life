#include <stdio.h>
#include <curand.h>
#include <curand_kernel.h>
#include <time.h>

extern "C"
__global__ void randinit(long n, unsigned char* cells, double chance) {

    long x = blockIdx.x * blockDim.x + threadIdx.x;
    long y = blockIdx.y * blockDim.y + threadIdx.y;

    curandState state;
    curand_init((unsigned long long)clock() + x + y, 0, 0, &state);
    if(x < n && y < n) {
        int chanceRoll = (int)curand(&state) % 255;
        if(x == 0 && y == 0) {
            printf("%d\n", chanceRoll);
        }
        cells[x + y * n] = (unsigned char) (chanceRoll & 0x000000FF);
    }
}

