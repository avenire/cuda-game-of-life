 __device__ int getIndexForCellAt(int x, int y, int n) {
    return x + n * y;
 }

extern "C"
__global__ void draw(uchar4* pos, unsigned int width, unsigned int height, int n, float zoomFactor, int d_x, int d_y, unsigned char* cells) {
    unsigned int px = blockIdx.x;
    unsigned int py = blockIdx.y;
    int2 displacement;
    displacement.x = d_x;
    displacement.y = d_y;



    int x = (((int)(px) - displacement.x));
    int y = (((int)(py) - displacement.y));
    x = ((x % (int)n) + n) % n;
    y = ((y % (int)n) + n) % n;

    unsigned char value = ((cells[x + n * y] >> px % 8) & 0x1) * 255;

    bool isNotOnBoundary = !((x == 0) || y == 0);
    int pixelId = (px * 8 + threadIdx.x + (py * 8 + threadIdx.y) * width);
    if(pixelId >= width * height) return;
    pos[pixelId].z = value;
    pos[pixelId].x = isNotOnBoundary ? value : 255;
    pos[pixelId].y = value;
}
